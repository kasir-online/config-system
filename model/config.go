package model

type Config struct {
	ConfigKey    string `json:"config_key"` //nolint:goimports
	ConfigValues string `json:"config_values"`
}
