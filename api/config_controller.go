package api

import (
	"config-system/httputil"
	"config-system/model"
	"config-system/service" //nolint:goimports
	"context"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/_examples/tutorial/mongodb/store"
)

type ConfigHandler struct {
	service service.ConfigService
}

func NewConfigHandler(service service.ConfigService) *ConfigHandler {
	return &ConfigHandler{service: service}
}

func (h *ConfigHandler) GetAll(ctx iris.Context) {
	config, err := h.service.GetAll(context.TODO())
	if err != nil {
		httputil.InternalServerErrorJSON(ctx, err, "Server was unable to retrieve all configs")
		return
	}

	if config == nil {
		config = make([]model.Config, 0)
	}

	_, _ = ctx.JSON(config)
}

func (h *ConfigHandler) Get(ctx iris.Context) {
	key := ctx.Params().Get("key")

	c, err := h.service.GetByKey(context.TODO(), key)

	if err != nil {
		if err == service.ErrNotFound {
			ctx.NotFound()
		} else {
			httputil.InternalServerErrorJSON(ctx, err, "Server was unable to retrieve config [%s]", key)
		}
		return
	}
	_, _ = ctx.JSON(c)
}

func (h *ConfigHandler) Add(ctx iris.Context) {
	c := new(model.Config)

	err := ctx.ReadJSON(c)

	if err != nil {
		_ = httputil.FailJSON(ctx, iris.StatusBadRequest, err, "Malformed request payload")
		return
	}

	err = h.service.Create(context.TODO(), c)

	if err != nil {
		httputil.InternalServerErrorJSON(ctx, err, "Server was unable to create a config")
		return
	}

	ctx.StatusCode(iris.StatusCreated)
	_, _ = ctx.JSON(c)
}

func (h *ConfigHandler) Update(ctx iris.Context) {
	key := ctx.Params().Get("key")

	var c model.Config
	err := ctx.ReadJSON(&c)
	if err != nil {
		_ = httputil.FailJSON(ctx, iris.StatusBadRequest, err, "Malformed request payload")
		return
	}

	err = h.service.Update(context.TODO(), key, c.ConfigValues)
	if err != nil {
		if err == store.ErrNotFound {
			ctx.NotFound()
			return
		}
		httputil.InternalServerErrorJSON(ctx, err, "Server was unable to update config [%s]", key)
		return
	}

	ctx.StatusCode(iris.StatusNoContent)
	_, _ = ctx.JSON(c)
}

func (h *ConfigHandler) Delete(ctx iris.Context) {
	key := ctx.Params().Get("key")

	err := h.service.Delete(context.TODO(), key)
	if err != nil {
		if err == store.ErrNotFound {
			ctx.NotFound()
			return
		}
		httputil.InternalServerErrorJSON(ctx, err, "Server was unable to delete Config [%s]", key)
		return
	}
}
