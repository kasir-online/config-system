package service

import (
	"config-system/model"
	"context"
)

type ConfigService interface {
	GetAll(ctx context.Context) ([]model.Config, error)
	GetByKey(ctx context.Context, key string) (model.Config, error)
	Create(ctx context.Context, config *model.Config) error
	Update(ctx context.Context, key string, value string) error
	Delete(ctx context.Context, key string) error
}
