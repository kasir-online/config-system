#FROM golang:latest
FROM golang:latest as build
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o server .

FROM alpine:latest
COPY --from=build /app/server /usr/local/bin/server
EXPOSE 20000
CMD ["/usr/local/bin/server"]