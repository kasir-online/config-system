package main

import (
	"config-system/api"
	"config-system/env"
	"config-system/service"
	"flag"
	"fmt"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/logger"
	recover2 "github.com/kataras/iris/v12/middleware/recover"
	"os"
)

func init() {
	envFileName := ".env"

	flags := flag.CommandLine
	flags.StringVar(&envFileName, "env", envFileName, "the env file which web app will use to extract its environment variables")
	_ = flags.Parse(os.Args[1:])

	env.Load(envFileName)
}

func main() {
	var (
		sess = session.Must(session.NewSessionWithOptions(session.Options{
			SharedConfigState: session.SharedConfigEnable,
		}))

		configService = service.NewConfigServiceAWS(dynamodb.New(sess))
	)

	app := iris.New()

	app.UseGlobal(recover2.New())
	app.UseGlobal(logger.New())
	app.Use(func(ctx iris.Context) {
		ctx.Next()
	})

	configAPI := app.Party("/api")
	{
		configHandler := api.NewConfigHandler(configService)
		configAPI.Get("/configs", configHandler.GetAll)
		configAPI.Put("/configs", configHandler.Add)
		configAPI.Get("/configs/{key}", configHandler.Get)
		configAPI.Post("/configs/{key}", configHandler.Update)
		configAPI.Delete("/configs/{key}", configHandler.Delete)
	}

	_ = app.Listen(fmt.Sprintf(":%s", env.Port), iris.WithOptimizations)
}
