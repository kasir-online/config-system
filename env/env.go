package env

import (
	"fmt"
	"github.com/joho/godotenv" //nolint:goimports
	"log"
	"os"
	"path/filepath"
	"strings"
)

var (
	Port string

	DSN string

	Database string
)

func parse() {
	Port = getDefault("PORT", "8080")
	DSN = getDefault("DSN", "mongodb://localhost:27017")
	Database = getDefault("Database", "config")

	log.Printf("• Port=%s\n", Port)
	log.Printf("• DSN=%s\n", DSN)
	log.Printf("• Database=%s\n", Database)
}

func Load(envFileName string) {
	if args := os.Args; len(args) > 1 && args[1] == "help" {
		_, _ = fmt.Fprintln(os.Stderr, "https://github.com/kataras/iris/blob/master/_examples/database/mongodb/README.md")
		os.Exit(-1)
	}

	envFiles := strings.Split(envFileName, ",")
	for _, envFile := range envFiles {
		if filepath.Ext(envFile) == "" {
			envFile += ".env"
		}

		if fileExists(envFile) {
			log.Printf("Loading environment variables from file: %s\n", envFile)

			if err := godotenv.Load(envFile); err != nil {
				panic(fmt.Sprintf("error loading environment variables from [%s]: %v", envFile, err))
			}
		}
	}

	parse()
}

func getDefault(key string, def string) string {
	value := os.Getenv(key)
	if value == "" {
		_ = os.Setenv(key, def)
		value = def
	}

	return value
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
