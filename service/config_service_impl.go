package service

import (
	"config-system/model"
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type configService struct {
	C *mongo.Collection
}

var _ ConfigService = (*configService)(nil)

func NewConfigService(collection *mongo.Collection) ConfigService { //nolint:goimports
	indexOpts := new(options.IndexOptions)
	indexOpts.SetName("configIndex").SetUnique(true).SetBackground(true).SetSparse(true)

	_, _ = collection.Indexes().CreateOne(context.Background(), mongo.IndexModel{
		Keys:    []string{"key"},
		Options: indexOpts,
	})
	return &configService{
		C: collection,
	}
}

func (c configService) GetAll(ctx context.Context) ([]model.Config, error) {
	cur, err := c.C.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	var results []model.Config

	for cur.Next(ctx) {
		if err = cur.Err(); err != nil {
			return nil, err
		}

		var elem model.Config
		err = cur.Decode(&elem)
		if err != nil {
			return nil, err
		}
		results = append(results, elem)
	}
	return results, nil
}

var ErrNotFound = errors.New("not found")

func (c configService) GetByKey(ctx context.Context, key string) (model.Config, error) {
	var config model.Config
	filter := bson.D{{Key: "key", Value: key}}

	err := c.C.FindOne(ctx, filter).Decode(&config)
	if err == mongo.ErrNoDocuments {
		return config, ErrNotFound
	}
	return config, nil
}

func (c configService) Create(ctx context.Context, config *model.Config) error {
	_, err := c.C.InsertOne(ctx, config)
	if err != nil {
		return err
	}
	return nil
}

func (c configService) Update(ctx context.Context, key string, value string) error {
	var config model.Config
	config.ConfigKey = key
	config.ConfigValues = value

	filter := bson.D{{Key: "key", Value: key}}

	update := bson.D{{Key: "$set", Value: config}}

	_, err := c.C.UpdateOne(ctx, filter, update)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return ErrNotFound
		}
		return err
	}

	return nil
}

func (c configService) Delete(ctx context.Context, key string) error {
	filter := bson.D{{Key: "key", Value: key}}

	_, err := c.C.DeleteOne(ctx, filter)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return ErrNotFound
		}
		return err
	}

	return nil
}
