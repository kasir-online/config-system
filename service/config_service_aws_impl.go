package service

import (
	"config-system/model"
	"context"
	"errors"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"os"
)

const keyString = "config_key"
const valueString = "config_values"

var tableName = new(string)

type configServiceAWS struct {
	Service *dynamodb.DynamoDB
}

func (c configServiceAWS) GetAll(ctx context.Context) ([]model.Config, error) {
	params := &dynamodb.ScanInput{
		TableName: tableName,
	}

	result, err := c.Service.Scan(params)
	if err != nil {
		return nil, err
	}

	var configs []model.Config
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, configs)
	if err != nil {
		_ = fmt.Errorf("failed to unmarshal Query result items, %v", err)
	}
	return configs, nil
}

func (c configServiceAWS) GetByKey(ctx context.Context, key string) (model.Config, error) {
	result, err := c.Service.GetItem(&dynamodb.GetItemInput{
		TableName: tableName,
		Key: map[string]*dynamodb.AttributeValue{
			keyString: {
				S: aws.String(key),
			},
		},
	})
	if err != nil {
		fmt.Println(err.Error())
		return model.Config{}, err
	}
	if result.Item == nil {
		msg := "Could Not Find Config '" + key + "'"
		return model.Config{}, errors.New(msg)
	}

	config := model.Config{}

	errorUnmarshal := dynamodbattribute.UnmarshalMap(result.Item, &config)
	if errorUnmarshal != nil {
		fmt.Println(errorUnmarshal.Error())
		return model.Config{}, errorUnmarshal
	}

	return config, nil
}

func (c configServiceAWS) Create(ctx context.Context, config *model.Config) error {

	av, err := dynamodbattribute.MarshalMap(config)
	if err != nil {
		fmt.Println("Got error marshalling new Config item:")
		fmt.Println(err.Error())
		os.Exit(1)
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: tableName,
	}

	_, err = c.Service.PutItem(input)
	if err != nil {
		fmt.Println("Got error calling PutItem:")
		fmt.Println(err.Error())
		return err
	}
	return nil
}

func (c configServiceAWS) Update(ctx context.Context, key string, value string) error {
	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":" + valueString: {
				S: aws.String(value),
			},
		},
		TableName: tableName,
		Key: map[string]*dynamodb.AttributeValue{
			keyString: {
				S: aws.String(key),
			},
		},
		ReturnValues:     aws.String("UPDATED_NEW"),
		UpdateExpression: aws.String("Set config_values = :" + valueString),
	}
	_, err := c.Service.UpdateItem(input)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}

func (c configServiceAWS) Delete(ctx context.Context, key string) error {
	input := &dynamodb.DeleteItemInput{
		TableName: tableName,
		Key: map[string]*dynamodb.AttributeValue{
			keyString: {
				S: aws.String(key),
			},
		},
	}
	_, err := c.Service.DeleteItem(input)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}

var _ ConfigService = (*configServiceAWS)(nil)

func NewConfigServiceAWS(service *dynamodb.DynamoDB) ConfigService {
	*tableName = "Config"
	return &configServiceAWS{service}
}
